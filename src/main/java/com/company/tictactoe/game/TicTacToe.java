package com.company.tictactoe.game;

public class TicTacToe
{
    TicTacToeLogic ticTacToeLogic;
    ReadFromUser readFromUser;
    public TicTacToe(){
        readFromUser = new ReadFromUser();
    }

    public String printResult(){
        if (ticTacToeLogic.isGameWon() && ticTacToeLogic.xOrO ==1) {

            String msg = "Wygrał gracz pierwszy X";
            System.out.println(msg);
            return msg;
        }else if (ticTacToeLogic.isGameWon()){
            String msg = "Wygrał gracz drugi O";
            System.out.println(msg);
            return msg;
        }else if
            (ticTacToeLogic.isBoardFilled()) {
            String msg = "Padł remis";
            System.out.println(msg);
            return msg;
        }
        return "";
    }

    public void play()
    {
        ticTacToeLogic = new TicTacToeLogic(readFromUser.readTableSize());
        while(!ticTacToeLogic.isBoardFilled()){
            ticTacToeLogic.printBoard();
            readFromUser.turnInfo(ticTacToeLogic);
            if(ticTacToeLogic.isGameWon()) break;
            readFromUser.readCoordinates(ticTacToeLogic);
        }
        printResult();
    }
}
