package com.company.tictactoe.game;

public class TicTacToeLogic {
    int n;
    int[][] poleGry;
    int countMoves = 0;
    int xOrO = 0;

    public TicTacToeLogic(int n) {
        this.n = n;
        poleGry = new int[n][n];
    }

    public boolean insertXorO(int x, int y) {
        if (poleGry[x][y] == 0) {
            poleGry[x][y] = xOrO + 1;
            xOrO += 1;
            xOrO = xOrO % 2;
            countMoves++;
            return true;
        }
        return false;
    }

    public boolean isGameWon() {
        boolean isWonH, isWonV;
        for (int i = 0; i < poleGry.length; i++) {
            isWonH = true;
            isWonV = true;
            for (int j = 0; j < poleGry.length; j++) {
                if (poleGry[i][j] == 0) isWonH = false;
                if (j - 1 >= 0 && poleGry[i][j - 1] != poleGry[i][j]) isWonH = false;
                if (poleGry[j][i] == 0) isWonV = false;
                if (j - 1 >= 0 && poleGry[j - 1][i] != poleGry[j][i]) isWonV = false;
            }
            if (isWonH || isWonV) return true;
        }
        boolean isWonD1 = true, isWonD2 = true;
        for (int i = 1; i < poleGry.length; i++) {
            if (poleGry[i][i] == 0) isWonD1 = false;
            if (poleGry[i][poleGry.length - i - 1] == 0) isWonD2 = false;
            if (poleGry[i][i] != poleGry[i - 1][i - 1]) isWonD1 = false;
            if (poleGry[i][poleGry.length - i - 1] != poleGry[i - 1][poleGry.length - i])
                isWonD2 = false;
        }
        if (isWonD1 || isWonD2) return true;
        return false;
    }

    public boolean isBoardFilled() {
        for (int i = 0; i < poleGry.length; i++) {
            for (int j = 0; j < poleGry.length; j++) {
                if (poleGry[i][j] == 0)
                    return false;
            }
        }
        return true;
    }

    public void printBoard() {
        for (int i = 0; i < poleGry.length; i++) {
            for (int j = 0; j < poleGry.length; j++) {
                if (poleGry[i][j] == 0) System.out.print(" _");
                else if (poleGry[i][j] == 1) System.out.print(" X");
                else if (poleGry[i][j] == 2) System.out.print(" 0");
            }
            System.out.println();
        }
    }
}
