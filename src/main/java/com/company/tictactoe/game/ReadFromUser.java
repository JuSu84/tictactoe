package com.company.tictactoe.game;

import java.util.Scanner;

public class ReadFromUser {

    Scanner sc = new Scanner(System.in);

    public void turnInfo(TicTacToeLogic tttl) {
        char turn;
        if (tttl.xOrO == 0) turn = 'X';
        else turn = 'O';
        System.out.println("Kolej gracza " + turn);
    }

    public int readTableSize() {
        System.out.println("Podaj wielkość pola gry miniumum 3");
        boolean isInputOk = false;
        int n = 0;
        do {
            String input;
            try {
                input = sc.nextLine();
                n = Integer.parseInt(input);
                if (n >= 3) {
                    isInputOk = true;
                    return n;
                } else
                    System.out.println("Podałeś za mały rozmiar tablicy. Podaj liczbę całkowitą większą lub równą 3:");
            } catch (NumberFormatException e) {
                System.out.println("Niewłaściwy format danych. Podaj liczbę całkowitą większą lub równą 3:");
            }
        } while (!isInputOk);
        return n;
    }

    public void readCoordinates(TicTacToeLogic kk) {
        int x, y;
        boolean isAssigned = false;
        while (!isAssigned) {
            System.out.println("Podaj numer wiersza");
            x = assignCoordinate(kk);
            System.out.println("Podaj numer kolumny");
            y = assignCoordinate(kk);
            if (kk.insertXorO(x, y)) {
                isAssigned = true;
            } else {
                kk.printBoard();
                System.out.println("Podane pole jest zajęte. Podaj jeszcze raz");
            }
        }
    }

    public int assignCoordinate(TicTacToeLogic kk) {
        int x = 0;
        boolean isInputOk = false;
        while (!isInputOk) {
            String input;
            try {
                input = sc.next();
                x = Integer.parseInt(input);
                if (x >= 0 && x < kk.n) {
                    isInputOk = true;
                } else {
                    kk.printBoard();
                    System.out.println("Podano indeks spoza dopuszcalnego zakresu (0," + (kk.n - 1) + ")");
                }
            } catch (NumberFormatException e) {
                kk.printBoard();
                System.out.println("Niewłaściwy format danych. Podaj liczbę całkowitą");
            }
        }
        return x;
    }
}
