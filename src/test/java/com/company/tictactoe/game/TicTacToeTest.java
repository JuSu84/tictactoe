package com.company.tictactoe.game;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TicTacToeTest {

    @Test
    void givenWhenThen() {
        //given
        TicTacToe ticTacToe = new TicTacToe();

        ticTacToe.ticTacToeLogic = new TicTacToeLogic(3);
        //forDraw
        int[][] tab = {{1, 2, 2}, {2, 1, 1}, {1, 1, 2}};
        ticTacToe.ticTacToeLogic.poleGry = tab;
        String s = ticTacToe.printResult();

        //forX
        int[][] tab1 = {{1, 1, 1},
                {1, 2, 1},
                {2, 0, 2}};
        ticTacToe.ticTacToeLogic.poleGry = tab1;
        String s1 = ticTacToe.printResult();
        //forO
        ticTacToe.ticTacToeLogic.xOrO++;
        int[][] tab2 = {{1, 2, 1}, {2, 2, 1}, {1, 2, 0}};
        ticTacToe.ticTacToeLogic.poleGry = tab2;
        String s2 = ticTacToe.printResult();
        //then
        Assertions.assertEquals("Padł remis", s);
        Assertions.assertEquals("Wygrał gracz pierwszy X", s2);
        Assertions.assertEquals("Wygrał gracz drugi O", s1);
    }


}