package com.company.tictactoe.game;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class ReadFromUserTest {

    @Test
    void givenBoardSize_whenIncorrectNumber_thenThrowException() {
        //given
        System.setIn(new ByteArrayInputStream("2".getBytes()));
        //when
        ReadFromUser readFromUser = new ReadFromUser();
        //then
        assertThrows(NoSuchElementException.class, () -> {
            readFromUser.readTableSize();
        });
    }
    @Test
    void givenBoardSize_whenNotNumber_thenThrowException() {
        //given
        System.setIn(new ByteArrayInputStream("abcd".getBytes()));
        //when
        ReadFromUser readFromUser = new ReadFromUser();
        //then
        assertThrows(NoSuchElementException.class, () -> {
            readFromUser.readTableSize();
        });
    }
    @Test
    void givenBoardSize_whenNumberCorrect_thenReturnSize() {
        //given
        System.setIn(new ByteArrayInputStream("3".getBytes()));
        ReadFromUser readFromUser = new ReadFromUser();
        //when
        int i1 = readFromUser.readTableSize();
        //then
        Assertions.assertEquals(3,i1);
    }

    @Test
    void givenBoard_whenOutOfRange_thenThrowException() {
        //given
        System.setIn(new ByteArrayInputStream("3".getBytes()));
        ReadFromUser readFromUser = new ReadFromUser();
        //when
        TicTacToeLogic ticTacToeLogic = new TicTacToeLogic(3);
        //then
        assertThrows(NoSuchElementException.class, () -> {
            readFromUser.assignCoordinate(ticTacToeLogic);
        });
    }
    @Test
    void givenBoard_whenNotNumber_thenThrowException() {
        //given
        System.setIn(new ByteArrayInputStream("bagd".getBytes()));
        ReadFromUser readFromUser = new ReadFromUser();
        //when
        TicTacToeLogic ticTacToeLogic = new TicTacToeLogic(3);
        //then
        assertThrows(NoSuchElementException.class, () -> {
            readFromUser.assignCoordinate(ticTacToeLogic);
        });
    }
    @Test
    void givenBoard_whenNumberOk_thenReturnCoordinate() {
        //given
        System.setIn(new ByteArrayInputStream("2".getBytes()));
        ReadFromUser readFromUser = new ReadFromUser();
        //when
        TicTacToeLogic ticTacToeLogic = new TicTacToeLogic(3);
        int i1 = readFromUser.assignCoordinate(ticTacToeLogic);
        //then
        Assertions.assertEquals(2,i1);
    }
}