package com.company.tictactoe.game;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TicTacToeLogicTest {

    @Test
    void givenInitialBoard_whenInsertXorO_thenInsertedTrue() {
        // given
        TicTacToeLogic ticTacToeLogic = new TicTacToeLogic(3);

        // when
        boolean insertedXorO = ticTacToeLogic.insertXorO(0, 0);
        boolean insertedSecondXorO = ticTacToeLogic.insertXorO(0, 0);

        // then
        Assertions.assertAll(
                () -> Assertions.assertTrue(insertedXorO, "Unable to insert X or O."),
                () -> Assertions.assertFalse(insertedSecondXorO, "Unable to insert X or O.")
        );
    }

    @Test
    void givenBoard_whenStateGame_thenCheckIfIsGameWon() {
        // given
        TicTacToeLogic ticTacToeLogic = new TicTacToeLogic(3);
        TicTacToeLogic ticTacToeLogic1= new TicTacToeLogic(3);
        //not won
        int[][] tab = {{1,2,2},{2,1,1},{1,1,2} };
        //horisontally
        int[][] tab1 = {{1, 1, 1},{1,2,1},{2,1,2}};
        int[][] tab2 = {{1,2,1},{1, 1, 1},{2,1,2}};
        int[][] tab3 = {{2,1,2},{1,2,1},{1, 1, 1}};

        //vertically
        int[][] tab4 ={{1,2,1},{2,2,1}, {1,2,2}};
        int[][] tab5 ={{1,2,1},{1,1,2},{1,2,1}};
        int[][] tab6 ={{1,2,1}, {2,1,1},{1,2,1}};
        //diagonally
        int[][] tab7 ={{1,2,2},{2,1,2},{2,2,1}};
        int[][] tab8 ={{1,1,2}, {1,2,2}, {2,2,1}};


                ticTacToeLogic.poleGry = tab;

        //when
        boolean isGameWon1 = ticTacToeLogic.isGameWon();
        boolean isGameWon2 = ticTacToeLogic1.isGameWon();
        ticTacToeLogic.poleGry = tab1;
        boolean isGameWon3 = ticTacToeLogic.isGameWon();
        ticTacToeLogic.poleGry = tab2;
        boolean isGameWon4 = ticTacToeLogic.isGameWon();
        ticTacToeLogic.poleGry = tab3;
        boolean isGameWon5 = ticTacToeLogic.isGameWon();
        ticTacToeLogic.poleGry = tab4;
        boolean isGameWon6 = ticTacToeLogic.isGameWon();
        ticTacToeLogic.poleGry = tab5;
        boolean isGameWon7 = ticTacToeLogic.isGameWon();
        ticTacToeLogic.poleGry = tab6;
        boolean isGameWon8 = ticTacToeLogic.isGameWon();
        ticTacToeLogic.poleGry = tab7;
        boolean isGameWon9 = ticTacToeLogic.isGameWon();
        ticTacToeLogic.poleGry = tab8;
        boolean isGameWon10 = ticTacToeLogic.isGameWon();


        //then
        Assertions.assertAll(
                () -> Assertions.assertFalse(isGameWon1, "Game is not won"),
                () -> Assertions.assertFalse(isGameWon2, "Game is not won"),
                () -> Assertions.assertTrue(isGameWon3, "Game is won horisontally"),
                () -> Assertions.assertTrue(isGameWon4, "Game is won horisontally"),
                () -> Assertions.assertTrue(isGameWon5, "Game is won horisontally"),
                () -> Assertions.assertTrue(isGameWon6, "Game is won vertically"),
                () -> Assertions.assertTrue(isGameWon7, "Game is won vertically"),
                () -> Assertions.assertTrue(isGameWon8, "Game is won vertically"),
                () -> Assertions.assertTrue(isGameWon9, "Game is won diagonally"),
                () -> Assertions.assertTrue(isGameWon10, "Game is won diagonally")
        );
    }

    @Test
    void givenBoard_whenIsBoardFilledOrNot_thenCheckResult() {
        //given
        TicTacToeLogic ticTacToeLogic = new TicTacToeLogic(3);
        //filled board
        int[][] tab = {{1,2,2},{2,1,1},{1,1,2} };
        //empty board
        int[][] tab1 = {{0,0,0},{0,0,0},{0,0,0} };
        //half filled
        int[][] tab2 = {{1,2,2},{2,1,1},{0,0,0}};

        //when
        ticTacToeLogic.poleGry = tab;
        boolean boardFilled = ticTacToeLogic.isBoardFilled();
        ticTacToeLogic.poleGry = tab1;
        boolean boardFilled1 = ticTacToeLogic.isBoardFilled();
        ticTacToeLogic.poleGry = tab2;
        boolean boardFilled2 = ticTacToeLogic.isBoardFilled();

        //then
        //then
        Assertions.assertAll(
                () -> Assertions.assertTrue(boardFilled, "Board is filled"),
                () -> Assertions.assertFalse(boardFilled1, "Board is not filled"),
                () -> Assertions.assertFalse(boardFilled2, "Board is not filled")
        );
    }
}