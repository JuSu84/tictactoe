package com.company.tictactoe.game;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void testIfSizeTableIncorect() {

        //given
        System.setIn(new ByteArrayInputStream("2".getBytes()));

        //then
        assertThrows(NoSuchElementException.class, () -> {
            Main.main(new String[0]);
        });
    }
}
